package connection;

import java.sql.Connection;
import java.sql.SQLException;

public class ConnectionUtils {

    public  static Connection getConnection() throws  ClassNotFoundException, SQLException{
        return MySqlConnUtils.getMySqlConnection();
    }

    public static void closeQuietle(Connection conn){

        try {
            conn.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void rollBackQuietly(Connection conn){

        try {
            conn.rollback();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
