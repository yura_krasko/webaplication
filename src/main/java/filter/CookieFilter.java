package filter;


import domain.UserAccount;
import util.DBUtils;
import util.FilterUtils;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

@WebFilter(filterName = "cookieFilter", urlPatterns = {"/*"})
public class CookieFilter implements Filter{

    public CookieFilter() {
    }

    @Override
    public void destroy() {

    }

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {


        HttpServletRequest req = (HttpServletRequest) request;
        HttpSession session = req.getSession();

        UserAccount userInSession = FilterUtils.getLoginedUser(session);

        if (userInSession != null){

            session.setAttribute("COOKIE_CHECKED", "CHEKED");

            chain.doFilter(request, response);

            return;
        }

        Connection conn = FilterUtils.getStoredConnection(request);

        String cheked = (String)session.getAttribute("COOKIE_CHEKED");

        if (cheked == null && conn != null){

            String userName = FilterUtils.getUserNameInCookie(req);

            try {
                UserAccount user = DBUtils.findUser(conn, userName);
                FilterUtils.storeLoginedUser(session, user);
            } catch (SQLException e) {
                e.printStackTrace();
            }

            session.setAttribute("COOKIE_CHECKED", "CHECKED");
        }

        chain.doFilter(request, response);
    }


}
