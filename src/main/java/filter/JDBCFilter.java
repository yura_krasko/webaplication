package filter;


import connection.ConnectionUtils;
import util.FilterUtils;

import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.*;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Collection;
import java.util.Map;

@WebFilter(filterName = "jdbcFilter", urlPatterns = {"/*"})
public class JDBCFilter implements Filter{

    public JDBCFilter() {
    }

    @Override
    public void init(FilterConfig fConfig) throws ServletException{
    }


    @Override
    public void destroy(){}

    private boolean needJDBC(HttpServletRequest request){

        System.out.println("JDBC filter");

        String  servletPath= request.getServletPath();
        String pathInfo = request.getPathInfo();

        String urlPattern = servletPath;

        if (pathInfo != null){
            urlPattern = servletPath + "/*";
        }

        Map<String, ? extends ServletRegistration> serletRegistrations = request.getServletContext().getServletRegistrations();

        Collection< ? extends ServletRegistration> values = serletRegistrations.values();
        for (ServletRegistration sr : values){
            Collection<String> mappings = sr.getMappings();
            if (mappings.contains(urlPattern)){
                return true;
            }
        }

        return false;
    }





    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest req = (HttpServletRequest) request;

        if (this.needJDBC(req)){

            System.out.println("Open Connection for: " + req.getServletPath());

            Connection conn = null;

            try{
                conn = ConnectionUtils.getConnection();

                conn.setAutoCommit(false);

                FilterUtils.storeConnection(request, conn);

                chain.doFilter(request, response);

                conn.commit();
            } catch (SQLException e) {
                e.printStackTrace();
            } catch (ClassNotFoundException e) {
                e.printStackTrace();

                ConnectionUtils.rollBackQuietly(conn);
                throw new ServletException();
            } finally {
                ConnectionUtils.closeQuietle(conn);
            }
        } else {

            chain.doFilter(request, response);
        }

    }
}
